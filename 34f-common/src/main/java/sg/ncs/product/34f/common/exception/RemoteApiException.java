package sg.ncs.product.34f.common.exception;

public class RemoteApiException extends RuntimeException {

    public RemoteApiException(String message) {
        super(message);
    }
}
